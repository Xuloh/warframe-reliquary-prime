const path = require('path');

module.exports = {
    sourceDir: path.resolve(__dirname, 'dist'),
    run: {
        startUrl: [
            'https://wf.xuerian.net/',
        ],
    },
    build: {
        overwriteDest: true,
    },
};
