# Warframe Reliquary Prime

A [Firefox][firefox] addon that adds some functionalities
to [Xuerian][xuerian]'s [Warframe][warframe] [reliquary][reliquary] that I felt were missing
(all credit goes to them for the amazing website).

Honestly, I would have made a pull request but it appears the project isn't open source so here we are.

I am not affiliated with Xuerian, just wanted to make my life a bit easier when using their site.

## Get the addon

You can get the addon from the [official Mozilla addon website][addon].

## How to build

This project uses [npm][npm] as a build tool.

Build files can be found in the `dist` directory.

The packaged addon can be found in the `web-ext-artifacts` directory.

Command | Usage
--- | ---
`npm install` | Install dependencies
`npm run start:firefox` | Start a firefox instance to test the addon
`npm run package` | Package the addon

[addon]: https://addons.mozilla.org/addon/warframe-reliquary-prime
[firefox]: https://www.mozilla.org/fr/firefox/new/
[warframe]: https://www.warframe.com/
[reliquary]: https://wf.xuerian.net/
[xuerian]: https://www.reddit.com/u/Xuerian
[npm]: https://docs.npmjs.com/
