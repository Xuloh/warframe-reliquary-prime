import {
    createButtonElement,
    createInputElement,
    createSelectElement,
} from './createDOMElements';
import { getRelicBoxes } from './dom';
import { filterByRewards, filterByTier } from './filter';
import { filter, tiers } from './model';

const ADDON_NAME = browser.runtime.getManifest().name;

console.log(ADDON_NAME, 'loading');

function updateBoxesDisplay() {
    const relicBoxes = getRelicBoxes();
    let resultCount = 0;
    relicBoxes.forEach((box) => {
        const display =
            filterByTier(box, filter.tier) &&
            filterByRewards(box, filter.reward);
        toggleDisplay(box, display);
        resultCount += display;
    });
    return resultCount;
}

/**
 * @param {Element} element
 * @param {boolean} display
 */
function toggleDisplay(element, display) {
    element.style.display = display ? '' : 'none';
}

const blockSpacing = '25px';
const itemSpacing = '5px';

const div = document.createElement('div');
div.style.marginBottom = blockSpacing;

const noMatchesDiv = document.createElement('div');
noMatchesDiv.append('No match :(');
noMatchesDiv.style.display = 'none';
noMatchesDiv.style.marginBottom = blockSpacing;

const selectTier = createSelectElement(
    tiers,
    (event) => {
        filter.tier = event.target.value;
        const results = updateBoxesDisplay();
        toggleDisplay(noMatchesDiv, results === 0);
    },
    'All'
);
selectTier.style.marginRight = itemSpacing;

const inputReward = createInputElement('Filter relic rewards', (event) => {
    filter.reward = event.target.value;
    const results = updateBoxesDisplay();
    toggleDisplay(noMatchesDiv, results === 0);
});
inputReward.style.marginRight = itemSpacing;

const refreshButton = createButtonElement('Refresh', updateBoxesDisplay);
refreshButton.style.marginRight = itemSpacing;

const clearButton = createButtonElement('Clear', () => {
    selectTier.value = '';
    inputReward.value = '';
    filter.tier = '';
    filter.reward = '';
    updateBoxesDisplay();
    toggleDisplay(noMatchesDiv, false);
});
clearButton.style.marginRight = itemSpacing;

div.append(selectTier, inputReward, refreshButton, clearButton);

document
    .getElementsByClassName('relics box-container')[0]
    .before(div, noMatchesDiv);

console.log(ADDON_NAME, 'loaded');
