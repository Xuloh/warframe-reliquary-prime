/**
 * A list of the different relic tiers
 * @readonly
 * @enum {string}
 */
const tiers = ['Lith', 'Meso', 'Neo', 'Axi', 'Requiem'];

const matchesTierFilter = 'matchesTierFilter';
const matchesRewardFilter = 'matchesRewardFilter';

const dataAttributes = {
    matchesTierFilter,
    matchesRewardFilter,
};

const filter = {
    tier: '',
    reward: ''
}

export { tiers, dataAttributes, filter };
