/**
 * Shows boxes of the given tier and hides the others
 * @param {Element} box
 * @param {string} tier the tier to show
 */
function filterByTier(box, tier) {
    const name = box.getElementsByClassName('name')[0].textContent;
    return name.startsWith(tier);
}

/**
 * Checks if all the terms of the given filter are included in the given string
 * @param {string} str the string to check
 * @param {string} filter the filter
 * @param {string} separator the separator used to separate filter terms
 * @returns true if all terms are included in the given string, false otherwise
 */
function matchesFilter(str, filter, separator = ' ') {
    const strLower = str.toLowerCase();
    const filterLower = filter.toLowerCase();
    return filterLower
        .split(separator)
        .every((term) => strLower.includes(term));
}

/**
 * Shows boxes of the given tier and hides the others
 * @param {Element} box
 * @param {string} tier the tier to show
 */
function filterByRewards(box, filter) {
    const rewards = Array.from(box.getElementsByClassName('reward wanted'));
    const match = rewards
        .map((reward) => reward.getElementsByClassName('name')[0].textContent)
        .some((reward) => matchesFilter(reward, filter));
    return match;
}

export { filterByTier, filterByRewards };
