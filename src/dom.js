/**
 * @returns the relic box elements
 */
function getRelicBoxes() {
    return Array.from(
        document
            .getElementsByClassName('reliquary')[0]
            .getElementsByClassName('relic box')
    );
}

export { getRelicBoxes };
