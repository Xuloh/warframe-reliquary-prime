/**
 * Creates an option element
 * @param {string} text the text of the option
 * @param {string} value the value of the option
 * @param {boolean} disabled if the option is disabled
 * @param {boolean} selected if the option is selected by default
 * @returns the created option element (not yet inserted in the dom)
 */
function createOptionElement(text, value, disabled = false, selected = false) {
    const option = document.createElement('option');
    option.append(text);
    option.value = value;
    option.disabled = disabled;
    option.selected = selected;
    return option;
}

/**
 * @param {string[]} options the options to add to the select
 * @param {(event: Event) => undefined} inputCallback the callback called on input events
 * @param {string?} defaultOption the default option (if any)
 * @returns the select elements that filters the relics by tier
 */
function createSelectElement(
    options,
    inputCallback = undefined,
    defaultOption = undefined
) {
    const select = document.createElement('select');

    if (defaultOption) {
        select.append(createOptionElement(defaultOption, '', false, true));
    }

    if (inputCallback) {
        select.addEventListener('input', inputCallback);
    }

    options
        .map((option) => createOptionElement(option, option))
        .forEach((option) => select.append(option));

    return select;
}

/**
 * @param {string?} placeholder the placeholder text (if any)
 * @param {(event: Event) => undefined} inputCallback the callback called on input events
 * @returns the input element that filters the relics by content
 */
function createInputElement(
    placeholder = undefined,
    inputCallback = undefined
) {
    const input = document.createElement('input');
    input.placeholder = placeholder;
    input.type = 'text';

    if (inputCallback) {
        input.addEventListener('input', inputCallback);
    }

    return input;
}

/**
 * @param {string} text
 * @param {(event: Event) => undefined} clickCallback
 * @returns
 */
function createButtonElement(text, clickCallback = undefined) {
    const button = document.createElement('button');
    button.append(text);
    if (clickCallback) {
        button.addEventListener('click', clickCallback);
    }
    return button;
}

export { createInputElement, createSelectElement, createButtonElement };
